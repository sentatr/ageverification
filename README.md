# Age Verification
---
[![pipeline status](https://gitlab.com/garybell/ageverification/badges/master/pipeline.svg)](https://gitlab.com/garybell/ageverification/-/commits/master) 
[![coverage report](https://gitlab.com/garybell/ageverification/badges/master/coverage.svg)](https://gitlab.com/garybell/ageverification/-/commits/master)
[![packagist version](https://img.shields.io/packagist/v/garybell/ageverification?style=flat-square)](https://img.shields.io/packagist/v/garybell/ageverification?style=flat-square)
[![package licence](https://img.shields.io/packagist/l/garybell/ageverification?label=License&style=flat-square)](https://img.shields.io/packagist/l/garybell/ageverification?label=License&style=flat-square)
[![supported PHP version](https://img.shields.io/packagist/php-v/garybell/ageverification?style=flat-square)](https://img.shields.io/packagist/php-v/garybell/ageverification?style=flat-square)  
  
**Facilitating legal requirements of verifying customers in age restricted industries**

I believe in a world where adhering to legislation shouldn't cost an arm and a leg. 
Hopefully this package can help some smaller businesses reduce their development time and costs to stay compliant.

This is a library which combines (or will in time) multiple age verification systems which can be implemented via common calls. 
Doing so should reduce friction if, or when, a change in AVS provider is requested by the business.

This library implements the services of the AVS providers in an unofficial capacity. 
Usage of the services requires a subscription with the relevant provider.

## Supported Age Verification Service Providers
The following AVS providers are supported
* [AgeChecked](https://www.agechecked.com/)

## Requirements
The following are required for the package to work:
* PHP 7.4
* cURL (plus php-curl)
* php-json 

## Installation
Installation is easiest via [composer](https://getcomposer.org/) using the command line. Run:  
`composer require garybell/ageverification`

## Usage
Each of the AVS implementations requires a data subject passing to it. 
This has to be an implementation of the AvsDataSubjectInterface, which has the following fields:
* firstName
* surname 
* building 
* street 
* postalCode
* country
* email 
* dateOfBirth

These are all available within GenericDataSubject if no alternative implementation is required.

Each of these are string data types, so they can be manipulated as required by the implementations. 
Some implementations have their own data subjects, which contain all of the above, plus additional fields for the services.

Create the data subject as:
```php
$dataSubject = new \GaryBell\AgeVerification\GenericDataSubject(
                    'firstname', 
                    'surname', 
                    'building', 
                    'street', 
                    'postcode', 
                    'country', 
                    'email', 
                    'dateOfBirth', 
                    );
```
This data subject is then used to build the payload for the age verification service.

The implementation of the age verification service needs to use the following steps:

```php
$avs = new Avs(); // e.g. new AgeChecked();
$avs->authenticate($config); // $config is an array of configuration requirements e.g. ['secretKey'=>'abc123']
$avs->buildAvsPayload($dataSubject);
if ($avs->payloadIsValid()) {
    $avs->submitAgeVerification();
    $avs->processVerificationResponse($avs->getRawResponse());
    if ($avs->isAgeVerified()) {
        // your logic here
    }
}
``` 
This allows each stage to be checked or audited with your code.
